#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import re

version = ''
with open('trend_2fc_session_plugin/__init__.py', 'r') as fd:
	version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
	                    fd.read(), re.MULTILINE).group(1)

if not version:
	raise RuntimeError('Cannot find version information')

setup(
	name='trend-2fc-session-plugin',
	version=version,
	description="""PyBpod GUI tren 2FC plugin""",
	author='Rachid Azizi',
	author_email='azi92rach@gmail.com',
	license='Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>',
	url='https://bitbucket.org/azi92rach/trend-2fc-session-plugin',

	include_package_data=True,
	packages=find_packages(exclude=['contrib', 'docs', 'tests', 'examples', 'deploy', 'reports']),

	package_data={'trend_2fc_session_plugin': [
		'resources/*.*',
	]
	},
)
