# Graphic Trend plugin for 2FC tasks #

A plugin for [Pyforms Generic Editor Welcome plugin](https://bitbucket.org/fchampalimaud/pybpod-gui-plugin).
It is useful to plot the last correct and wrong decisions.

It is tested on Ubuntu with Python 3.

## How to install:

### Plugin
1. On your Desktop, create a new folder on your favorite location (e.g. "plugins")
2. Move this plugin folder to that location
3. Open GUI
4. Select Options > Edit user settings
5. Add the following info:

```python
    GENERIC_EDITOR_PLUGINS_PATH = 'PATH TO PLUGINS FOLDER' # USE DOUBLE SLASH FOR PATHS ON WINDOWS
    GENERIC_EDITOR_PLUGINS_LIST = [
        (...other plugins...),
        'trend_2FC_session_plugin',
    ]
```

6. Save
7. Restart GUI
8. Select Tools and open the plugin

## How to use

# Protocol
1. The protocol needs to have a list containing 0 for LEFT side and 1 for RIGHT side. 
2. The list has to be saved in the Bpod class with the name "VECTOR_CHOICE". 

Example: 
```python
    
from pybpodapi.protocol import Bpod, StateMachine

my_bpod = Bpod()

vec_choice = [0,1,1,1,1,0,0,0,1,1]
my_bpod.register_value('VECTOR_CHOICE', vec_choice)

nTrials = 10
for i in range(nTrials):  # Main loop
	print('Trial: ', i + 1)

	thisTrialType = vec_choice[i] 
	if thisTrialType == 0: # left
		stimulus = Bpod.OutputChannels.PWM1  # set stimulus channel for trial type 1
		leftAction = 'Reward'
		rightAction = 'Punish'
		rewardValve = 1
	elif thisTrialType == 1: #right
		stimulus = Bpod.OutputChannels.PWM3  # set stimulus channel for trial type 1
		leftAction = 'Punish'
		rightAction = 'Reward'
		rewardValve = 3

	sma = StateMachine(my_bpod)

	sma.add_state(
		state_name='WaitForPort2Poke',
		state_timer=1,
		state_change_conditions={Bpod.Events.Tup: 'FlashStimulus'},
		output_actions=[(Bpod.OutputChannels.PWM2, 255)])
	sma.add_state(
		state_name='FlashStimulus',
		state_timer=0.1,
		state_change_conditions={Bpod.Events.Tup: 'WaitForResponse'},
		output_actions=[(stimulus, 255)])
	sma.add_state(
		state_name='WaitForResponse',
		state_timer=1,
		state_change_conditions={Bpod.Events.Port1In: leftAction, Bpod.Events.Port3In: rightAction},
		output_actions=[])
	sma.add_state(
		state_name='Reward',
		state_timer=0.1,
		state_change_conditions={Bpod.Events.Tup: 'exit'},
		output_actions=[(Bpod.OutputChannels.Valve, rewardValve)])  # Reward correct choice
	sma.add_state(
		state_name='Punish',
		state_timer=3,
		state_change_conditions={Bpod.Events.Tup: 'exit'},
		output_actions=[(Bpod.OutputChannels.LED, 1), (Bpod.OutputChannels.LED, 2), (Bpod.OutputChannels.LED, 3)])  # Signal incorrect choice

	my_bpod.send_state_machine(sma)  # Send state machine description to Bpod device

	print("Waiting for poke. Reward: ", 'left' if thisTrialType == 0 else 'right')

	my_bpod.run_state_machine(sma)  # Run state machine

	print("Current trial info: {0}".format(my_bpod.session.current_trial))

my_bpod.close()  # Disconnect Bpod


```


To open the plugin, click with the right click of mouse on the session and select 'Trend' plugin.
![trend-2FC-session-plugin](images/trendOp.png)

With the update button you can fix the last n number of points to plot. You can set -1 to show all of them. 

![trend-2FC-session-plugin_win](images/trendWin.png)

If you are running the session you should see the following figure:
    
    Colors:
        - Gray: the future choises
        - Blue: the next trial
        - Red: wrong answare
        - Green: correct answare 

![trend-2FC-session-plugin_win](images/trendWinRealTime.png)
    

## License
This is Open Source software. We use the `GNU General Public License version 3 <https://www.gnu.org/licenses/gpl.html>`
