import os
import traceback
import functools

def log(func):
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as error:
            traceback.print_tb(error.__traceback__)
            print(traceback.format_exc())
    return wrapper 

def method_log(func):
    @functools.wraps(func)
    def wrapper(self, *args, **kwargs):
        try:
            return func(self, *args, **kwargs)
        except Exception as error:
            traceback.print_tb(error.__traceback__)
            print(traceback.format_exc())
    return wrapper 
