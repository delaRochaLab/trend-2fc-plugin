"""
Presets file for the trend plugin. Each task is a dictionary with the following fields:
    - STATES: (dict) the states that you want to see in the trend, together with their color.
    The tasks should be designed so as the trials always exit in a state which indicates the 
    result of that trial, and these exits states shouldn't repeat in the same trial
    or appear together with another exit state. Any trial that doesn't contain one of
    these states WILL BE IGNORED, and any state that contains more than one will count
    as more than one trial.
    - ACC_UP: (list) the states that make the accuracy go up. 
    - ACC_DOWN: (list) the states that make the accuracy go down.
    - ACC_NEUTRAL: (list) the states that don't affect the accuracy.
    - WINDOW_SIZE: (int) size of the window for the display of the averages.
    - SILENCE_TRIALS_MARKER: (matplotlib marker)* the marker that will indicate deliberate missing trials (Dani's tasks).
    - MISSING_SOUNDS_MARKER: (matplotlib marker)* the marker that will indicate missing sounds by those
        boxes fitted with the sound detector. 
Each preset is selectable inside the trend window. Add as many presets as you want, 
just be mindful of the commas between them.

* See the possibilities here: https://matplotlib.org/3.1.0/api/markers_api.html

"""

PRESETS = {
    "P4": {
        "STATES": {'Reward': 'green', 'Punish': 'red', 'Invalid': 'gray'},
        "ACC_UP": ["Reward"],
        "ACC_DOWN": ["Punish", "Invalid"],
        "ACC_NEUTRAL": [],
        "WINDOW_SIZE": 20,
        "SILENCE_TRIALS_MARKER": '*',
        "MISSING_SOUNDS_MARKER": 'x',
        "THIRD_PLOT": 'False'
    },
    "P1":{
        "STATES": {'Reward': 'green', 'Punish': 'red', 'Miss': 'black', 'Invalid': 'gray'},
        "ACC_UP": ["Reward"],
        "ACC_DOWN": ["Punish", "Invalid"],
        "ACC_NEUTRAL": ["Miss"],
        "WINDOW_SIZE": 20,
        "SILENCE_TRIALS_MARKER": '*',
        "MISSING_SOUNDS_MARKER": 'x',
        "THIRD_PLOT": 'False'
    },
    "P4_OPTO": {
        "STATES": {'Reward': 'green', 'RewardLight': 'green', 'Punish': 'red', 'PunishLight': 'red', 'Invalid': 'gray'},
        "ACC_UP": ['Reward', 'RewardLight'],
        "ACC_DOWN": ['Punish', 'PunishLight', 'Invalid'],
        "ACC_NEUTRAL": [],
        "WINDOW_SIZE": 20,
        "SILENCE_TRIALS_MARKER": '*',
        "MISSING_SOUNDS_MARKER": 'x',
        "THIRD_PLOT": 'True'
    }
}
