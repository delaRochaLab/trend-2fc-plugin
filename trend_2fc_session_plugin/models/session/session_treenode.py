# !/usr/bin/python
# -*- coding: utf-8 -*-

import logging

from pyforms import conf

from AnyQt.QtGui import QIcon
from trend_2fc_session_plugin.session_trend import SessionTrend

logger = logging.getLogger(__name__)


class SessionTreeNode(object):
    def create_treenode(self, tree):
        """

        :param tree:
        :return:
        """
        node = super(SessionTreeNode, self).create_treenode(tree)

        self.sessiontrend_action = tree.add_popup_menu_option(
            'Trend',
            self.open_sessiontrend_win,
            item=self.node,
            icon=QIcon(conf.SESSIONTREND_PLUGIN_ICON)
        )

        self.sessiontrend_detached_action = tree.add_popup_menu_option(
            'Trend (detached)',
            self.open_sessiontrend_win_detached,
            item=self.node,
            icon=QIcon(conf.SESSIONTREND_PLUGIN_ICON)
        )

        return node

    def node_double_clicked_event(self):
        super(SessionTreeNode, self).node_double_clicked_event()
        self.open_sessiontrend_win()

    def open_sessiontrend_win(self):
        if self.is_running and self.setup.detached: # if the detached flag is anable don't show the data
            return
        self.load_contents()
        # does not show the window if the detached window is visible
        if hasattr(self, 'sessiontrend_win_detached') and self.sessiontrend_win_detached.visible: 
            return
        if not hasattr(self, 'sessiontrend_win'):
            self.sessiontrend_win = SessionTrend(self)
            self.sessiontrend_win.show()
            self.sessiontrend_win.subwindow.resize(*conf.SESSIONTREND_PLUGIN_WINDOW_SIZE)
        else:
            self.sessiontrend_win.show()

        self.sessiontrend_action.setEnabled(False)
        self.sessiontrend_detached_action.setEnabled(False)

    def open_sessiontrend_win_detached(self):
        if self.is_running and self.setup.detached:
            return
        self.load_contents()
        # does not show the window if the attached window is visible
        if hasattr(self, 'sessiontrend_win') and self.sessiontrend_win.visible: 
            return
        if not hasattr(self, 'sessiontrend_win_detached'):
            self.sessiontrend_win_detached = SessionTrend(self)
            self.sessiontrend_win_detached.show(True)
            self.sessiontrend_win_detached.resize(*conf.SESSIONTREND_PLUGIN_WINDOW_SIZE)
        else:
            self.sessiontrend_win_detached.show(True)

        self.sessiontrend_action.setEnabled(False)
        self.sessiontrend_detached_action.setEnabled(False)
    
    def close_window(self):
            if hasattr(self, 'sessiontrend_win'): self.mainwindow.mdi_area -= self.sessiontrend_win

    def remove(self):
        self.close_window()
        super(SessionTreeNode, self).remove()

    @property
    def name(self):
        return super(SessionTreeNode, self.__class__).name.fget(self)

    @name.setter
    def name(self, value):
        super(SessionTreeNode, self.__class__).name.fset(self, value)
        if hasattr(self, 'sessiontrend_win'): self.sessiontrend_win.title = value
