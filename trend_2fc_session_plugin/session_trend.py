# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" 
Main window of the trend plugin. This plugin uses a user preset file (trend_settings.py)
that has to be configured before using. 
"""

import logging

from pyforms import conf

import numpy as np

from AnyQt.QtWidgets import QMessageBox

from AnyQt.QtGui import QColor, QBrush
from AnyQt.QtCore import QTimer

from pybpodapi.session import Session
from pyforms.basewidget import BaseWidget
from pyforms.controls import ControlProgress
from pyforms.controls import ControlText
from pyforms.controls import ControlMatplotlib
from pyforms.controls import ControlLabel
from pyforms.controls import ControlButton
from pyforms.controls import ControlCheckBox
from pyforms.controls import ControlCombo

from matplotlib.lines import Line2D

from trend_2fc_session_plugin.myData import strToData
from trend_2fc_session_plugin.trend_settings import PRESETS
from trend_2fc_session_plugin.exc_manager import log, method_log

import logging
import traceback
import datetime

logger = logging.getLogger(__name__)

class SessionTrend(BaseWidget):
    """ Plugin main window """

    COUNTER = 0  # Counter that tracks the last read position of the session data.

    @method_log
    def __init__(self, session):
        BaseWidget.__init__(self, session.name)

        self.layout().setContentsMargins(5, 5, 5, 5) # Set graphic margin
        self.session = session # Session Class of the pybpod to get data
        # Load the presets:
        self.presets = PRESETS
        # ------------ Form creation: -------------------------------------------
        self._graph = ControlMatplotlib('Graph')
        self._updatebutton = ControlButton('Update')
        self._valGraph = ControlText('Values to show. [-1 for all]: ', default='100', helptext='Values that are being plotted in the graph.')
        self._title_trend = ControlLabel('Trend of 2FC task.')
        self._coh_checkbox = ControlCheckBox('Show coherences')
        self._presets_combo = ControlCombo('Presets: ', helptext='The settings for this session.')
        self._plot_mode = ControlCombo('Plot mode: ', helptext='Choose between different types of plotting intervals.')
        self._plot_mode.changed_event = self.refresh_mode
        self._axis_start = ControlText('Start: ', default='0', helptext='In Range mode, select the first trial to plot.')
        self._axis_size = ControlText('Length', default='100', helptext='Select how many trials you want to display.')
        self._show_missing_sounds = ControlCheckBox(' Sound detection', helptext='Enable or disable sound detection. It needs the detection board!')
        self.start, self.length = int(self._axis_start.value), int(self._axis_size.value)
        # Form position:
        self._formset = [
            '_title_trend', '=', ('_presets_combo',  '||',  '_coh_checkbox', '||', '_show_missing_sounds',  '||', ' ' , '_updatebutton'),
            ('_plot_mode', '_axis_start', '_axis_size', ' ', ' ', ' '),
            '=', '_graph']
        # ----------------------------------------------------------------------
        # Populate the presets combo box:
        for key in PRESETS:
            self._presets_combo.add_item(key, key)
        # Populate the plot mode combo box:
        for choice in ['Last', 'First', 'Range']:
            self._plot_mode.add_item(choice, choice)
        # Assign an action to the button
        self._updatebutton.value = self.__updateAction
        # Class that processes the data:
        self.processor = strToData(self.current_settings)
        self._graph.value = self.__on_draw  # Define the method that will modify the graph.
        # Set the timer to recall the update function:
        self._timer = QTimer()
        self._timer.timeout.connect(self.update)
        # Vector to contain the choices vector that will be extracted from the data:
        self.reward_side = None
        # Vector that will contain the trial indices for the detection of missing sounds:
        self.trial_indices = []
        # ---------- Try to read session metadata: ------------------
        dataframe = self.session.data
        try:
            self._setup_name = dataframe[dataframe.MSG == 'SETUP-NAME']['+INFO'].iloc[0]
        except IndexError:
            self._setup_name = "Unknown box"
        try:
            self._session_started = dataframe[dataframe.MSG == 'SESSION-STARTED']['+INFO'].iloc[0].split()[1][0:8]
        except IndexError:
            self._session_started = "Unknown time"
        try:
            self._subject_name = dataframe[dataframe.MSG == 'SUBJECT-NAME']['+INFO'].iloc[0]
            self._subject_name = self._subject_name[1:-1].split(',')[0].strip("'")
        except IndexError:
            self._subject_name = "Unknown subject"
        
        # Set the session title:
        self.title = f'{self._setup_name} / {self._session_started} / {self._subject_name}'

    # ---------------------- BUTTON ACTIONS ------------------------------
    def refresh_mode(self):
        """
        Action tied to the plot mode ControlCombo "changed" event. It disables the start input
        field when not in "Range" mode.
        """
        mode = self._plot_mode.value
        self._axis_start.enabled = bool(mode == 'Range')

    def __updateAction(self):
        """ 
        Method tied to the Update button; it does type checking on the start and length fields
        and calls for the graph to be refreshed.
        """
        if self.current_plot_mode == 'Range':
            try:
                start = int(self._axis_start.value)
                if start < 1: 
                    raise ValueError
            except ValueError:
                self.warning('Only integers greater than zero are allowed in the input fields.', 'ValueError')
            else:
                self.start = start
        try:
            length = int(self._axis_size.value)
            if length < 1: 
                raise ValueError
        except ValueError:
            self.warning('Only integers greater than zero are allowed in the input fields.', 'ValueError')
        else:
            self.length = length
        self.COUNTER = 0
        self.trial_indices = []
        self.processor = strToData(self.current_settings)
        self.update()
    # ------------------------------------------------------------------

    @method_log
    def read_message_queue(self):
        """ 
        Main loop: read the session data, send it to myData for processing and
        finally update the graph.
        """
        try:
            # --------- Init reward side: -------------------------------
            if self.reward_side is None:  # If the vector choice was not set, we get the last we find (iloc[-1])
                self.reward_side = self.session.data[self.session.data.MSG == "REWARD_SIDE"]['+INFO'].iloc[-1]
                self.processor.add_reward_side(self.reward_side)  # Pass it to myData.
            # -------------- READ SESSION DATA --------------------------------
            data = self.session.data[self.COUNTER:] 
            self.COUNTER += len(data) 
            # ----------------- COHERENCES ----------------------------
            # Gather the coherences and transform them into evidences. Coherences go from 0 (left) to 1 (right),
            # while evidences go from -1 (right) to 1 (left):
            coherences = data.query("MSG == 'coherence01'")['+INFO'].values.astype(float)
            evidences = (-2 * coherences + 1).tolist()
            # Send them to the processor class:
            self.processor.add_evidences(evidences)
            # ----------------- ZT VALUES AND LIGHT (FOR OPTO TASK) ----------------------------

            if self.processor.zt_threshold == 0:
                try:
                    self.processor.zt_threshold = data.query("MSG == 'zt_threshold'")['+INFO'].values.astype(float)[0]
                    #print('zt_threshold', self.processor.zt_threshold)
                except:
                    self.processor.zt_threshold = 0

            try:
                zts = data.query("MSG == 'zt' and TYPE=='VAL'")['+INFO'].values
                lights = data.query("MSG == 'light_on_trial' and TYPE=='VAL'")['+INFO'].values
                trials = list(range(len(lights)))
                #print('zts', zts)
                #print('lights', lights)
                #print('trials', trials)
                #print('')
                zt_lights = [abs(float(zt)) for zt, light in zip(zts, lights) if light == 'True']
                trial_lights = [trial for trial, light in zip(trials, lights) if light == 'True']
                zt_no_lights = [abs(float(zt)) for zt, light in zip(zts, lights) if light == 'False']
                trial_no_lights = [trial for trial, light in zip(trials, lights) if light == 'False']
            except:
                zt_lights = []
                trial_lights = []
                zt_no_lights = []
                trial_no_lights = []

            # Send them to the processor class:
            self.processor.zt_lights += zt_lights
            self.processor.zt_no_lights += zt_no_lights

            try:
                tlight = self.processor.trial_lights[-1] + 1
            except:
                tlight = 0
            try:
                tnolight = self.processor.trial_no_lights[-1] + 1
            except:
                tnolight = 0

            t = max(tlight, tnolight)
            trial_lights = [value + t for value in trial_lights]
            trial_no_lights = [value + t for value in trial_no_lights]

            self.processor.trial_lights += trial_lights
            self.processor.trial_no_lights += trial_no_lights

            #print('zt lights', self.processor.zt_lights)
            #print('zt no lights', self.processor.zt_no_lights)
            #print('trial lights', self.processor.trial_lights)
            #print('trial no lights', self.processor.trial_no_lights)
            #print('')

            # ------------ REWARD_SIDE LIVE UPDATE --------------------
            # If a new reward side comes, we just swap it with the old - the task has to be designed to
            # ensure that the past trials remain the same inside the vector.
            try:    
                live_reward_side = self.session.data[self.session.data.MSG == "REWARD_SIDE"]['+INFO'].iloc[-1]
            except IndexError:  # No new reward_side.
                pass
            else:
                self.processor.add_reward_side(live_reward_side)
            # ------------ SILENCES --------------------
            # For Dani's task, in which some trials are silenced and have to be displayed with an 'x'.
            # Silences are registered as a value with 1 (silence) or 0 (with sound).
            silences = data.query("TYPE=='VAL' and MSG=='silence'")['+INFO'].values.astype(float)
            # We convert zeroes to NaN's for easier plotting later:
            silences[silences==0] = np.nan
            silences = silences.tolist()
            # Send them to the processor class:
            self.processor.add_silences(silences)            
            # ---------------- STATES --------------------------------
            # Gather the exit state of the trial or trials:
            exit_states = self.current_settings['STATES']
            print(exit_states)
            states = data.query("TYPE=='STATE' and MSG in @exit_states")
            print(states)
            # Be careful with the NaN fields! While the session is live, the fields are strings, and NaNs
            # cannot be eliminated with dropna(). When the session has ended, however, the data is converted
            # into a Pandas dataframe and then dropna() works as expected. Therefore, a solution that works
            # for both situations has to (1) compare the field with the 'nan' string for live sessions and
            # (2) use dropna() for finished sessions.
            states = states[states['+INFO'] != 'nan'].dropna(subset=['+INFO'])
            states = states['MSG']
            # Send the states to the processor class:
            self.processor.process_past_trials(states)
            # ---------- MISSING SOUNDS ----------------
            # Cut the dataframe into chunks and send them to processor to see if
            # there are missing BNC transitions. We are sending the dataframe back again,
            # and thus this is a bit inefficient ... in the future this should be joined together with
            # the previous code.
            
            new_indices = data.query("TYPE=='TRIAL' and MSG=='New trial'").index.tolist()
            if not self.trial_indices:
                if len(new_indices) > 1:
                    self.processor.sound_detection(self.session.data[new_indices[0]:new_indices[-1]])
            else:
                if len(new_indices) > 1:
                    self.processor.sound_detection(self.session.data[self.trial_indices[-1]:new_indices[-1]])
            self.trial_indices += new_indices
            # --------- Update the graph: -------------- 
            self._graph.draw()
        except Exception as err:
            if hasattr(self, '_timer'):
                self._timer.stop()
            # logger.error(str(err), exc_info=True)
            traceback.print_tb(err.__traceback__)
            with open('plugins_logs.txt', 'a') as logfile:
                    logfile.write(str(datetime.datetime.now())+'\n')
                    logfile.write(traceback.format_exc())
                    logfile.write('-'*25)
            QMessageBox.critical(self, "Error",
                                 "An error in the trend plugin occurred. Check logs for details.")

    def update(self):
        """ 
        This method is tied to a QTimer() that keeps calling it every X seconds, 
        where X is defined in the settings.py file (and imported here as 'conf'). It 
        commands the main loop to read session data only if new data is available.
        """
        if not self.session.is_running: self._timer.stop()
        if len(self.session.data) > self.COUNTER:
            self.read_message_queue()

    @method_log
    def __on_draw(self, figure):
        """ 
        Method that redraws the figure.
        """
        third_plot = False
        try:
            if self.current_settings['THIRD_PLOT'] == 'True':
                third_plot = True
        except:
            pass
        try:
            # --------- GRAPH BOUNDARIES -----------------
            axis_size = self.length
            axis_start = self.start
            if self.current_plot_mode == 'First':
                start, end = 0, axis_size
            elif self.current_plot_mode == 'Last':
                start = self.processor.LAST_TRIAL - axis_size // 2
                end = self.processor.LAST_TRIAL + axis_size // 2
                if start < 0:
                    start, end = 0, axis_size
            else: # Range:
                start, end = axis_start, axis_start + axis_size
            self.processor.start, self.processor.end = start, end  # Send the boundaries to the processor class.
            # Clean the figure, and create 2 subplots:
            figure.clear()

            if third_plot:
                axes1 = figure.add_subplot(311)
                axes2 = figure.add_subplot(312)
                axes3 = figure.add_subplot(313)
            else:
                axes1 = figure.add_subplot(211)
                axes2 = figure.add_subplot(212)

            # ------------- FIRST PLOT: trial results ------------------------------
            # coh is a boolean that will be True when the session has coherences and the 
            # user has checked the "show coherences" checkbox.  
            coh = self.processor.evidences and self._coh_checkbox.value
            # Ask for the results and the reward side to fill the rest of the screen:
            x, vals, colors, silences = self.processor.trial_results(coh)
            print(x, vals, colors)
            x_rew, reward_side = self.processor.cut_reward_side(len(x))
            if silences:
                compl_silences = np.array([1 if np.isnan(elem) else np.nan for elem in silences])
                silences = np.array(vals) * np.array(silences)
                vals = np.array(vals) * compl_silences
                axes1.scatter(x, silences, s=12, c=colors, marker=self.current_settings['SILENCE_TRIALS_MARKER'])
            if self._show_missing_sounds.value and self.processor.missing_sounds:
                missing_indices = np.isin(x, np.intersect1d(x, self.processor.missing_sounds)).nonzero()[0]
                values = np.take(np.array(vals), missing_indices)
                vals = np.array(vals)
                np.put(vals, missing_indices, np.array([np.nan]))
                axes1.scatter(missing_indices, values, s=12, c=colors, marker=self.current_settings['MISSING_SOUNDS_MARKER'])
            # Plot everything:
            axes1.scatter(x_rew, reward_side, s=10, c='gray')
            axes1.scatter(x, vals, s=10, c=colors)
            #  ----- First plot configuration: -----------
            axes1.set_ylim([-1.1, 1.1])
            axes1.set_xlim([start, end])
            axes1.set_yticklabels(['R', 'L'])
            axes1.set_yticks([0], minor=True)
            axes1.set_yticks([-1, 1], minor=False)
            axes1.set_axisbelow(True)  # Put the grid behind plot elements.
            axes1.grid(which='major', linestyle='dashed', alpha=0.6)
            if coh:  # Show only the dashed line on y=0 when in "coherence mode":
                axes1.grid(which='minor', linestyle='dashed', linewidth=1, color='black', alpha=0.6)
            # ----------------- SECOND PLOT: performances ------------------------
            L_x, L_vals = self.processor.left_performance
            R_x, R_vals = self.processor.right_performance
            total_x, total_vals = self.processor.total_performance
            axes2.scatter(L_x, L_vals,  facecolors='none', edgecolors='cyan', s=15, alpha=0.9)
            axes2.scatter(R_x, R_vals,  facecolors='none', edgecolors='magenta', s=15, alpha=0.9)
            axes2.scatter(total_x, total_vals,  facecolors='none', edgecolors='black', s=15, alpha=0.9)
            # ------------------ Second plot configuration: -----------------
            axes2.set_ylabel('Accuracy [%]')
            axes2.set_xlabel('Trials')
            axes2.set_ylim([0, 1.1])
            axes2.set_xlim([start, end])
            axes2.set_yticks(list(np.arange(0, 1.1, 0.1)))
            axes2.set_yticklabels(['0', '', '','','','50', '','','','','100'])
            axes2.grid(linestyle='dashed', alpha=0.6)

            if third_plot:
                # ----------------- THIRD PLOT: performances ------------------------
                axes3.scatter(self.processor.trial_lights, self.processor.zt_lights, facecolors='blue',
                              edgecolors='blue', s=15, alpha=0.9)
                axes3.scatter(self.processor.trial_no_lights, self.processor.zt_no_lights, facecolors='none',
                              edgecolors='black', s=15, alpha=0.9)
                axes3.hlines(y=self.processor.zt_threshold, xmin=start, xmax=end, linewidth=2, color='r')
                # ------------------ Third plot configuration: -----------------
                axes3.set_ylabel('Zt')
                axes3.set_xlabel('Trials')
                axes3.set_ylim([0, 1.1])
                axes3.set_xlim([start, end])
                axes3.set_yticks(list(np.arange(0, 1.1, 0.1)))
                axes3.set_yticklabels(['0', '', '','','','50', '','','','','100'])
                axes3.grid(linestyle='dashed', alpha=0.6)
            # ----------- LEGEND: -------------
            # self.legend is a property below.
            #axes2.legend(loc='upper center',
            #        bbox_to_anchor=(0.5, -0.4),
            #        handles=self.legend,
            #        ncol=3,
            #        fontsize=9,
            #        fancybox=True,
            #        shadow=True)
            figure.tight_layout()
        except Exception as err:
            print(err)

    def show(self, detached=False):
        """ 
        Method used to show the windows.
        """
        # Prevent the call to be recursive because of the mdi_area
        if not detached:
            if hasattr(self, '_show_called'):
                BaseWidget.show(self)
                return
            self._show_called = True
            self.mainwindow.mdi_area += self
            del self._show_called
        else:
            BaseWidget.show(self)
        self._stop = False  # flag used to close the gui in the middle of a loading
        self.read_message_queue() # Read all the data and update the plot  
        if not self._stop and self.session.is_running: # if it is in real time
            self._timer.start(conf.SESSIONTREND_PLUGIN_REFRESH_RATE)

    def before_close_event(self):
        """ 
        Method called before closing the windows. 
        """
        self._timer.stop()
        self._stop = True
        self.session.sessiontrend_action.setEnabled(True)
        self.session.sessiontrend_detached_action.setEnabled(True)

    @property
    def mainwindow(self):
        return self.session.mainwindow

    @property
    def title(self):
        return BaseWidget.title.fget(self)

    @title.setter
    def title(self, value):
        BaseWidget.title.fset(self, value)

    @property
    def current_settings(self):
        """
        Returns a dictionary with the settings for the current task as selected by the user
        by means of the preset combobox.
        """
        return PRESETS[self._presets_combo.value]
     
    @property
    def current_plot_mode(self):
        """
        Returns a dictionary with the current task's plot mode (First, Last or Range).
        """
        return self._plot_mode.value

    @property
    def legend(self):
        """
        Define the legend artists (always the same, independent of the session settings).
        """
        config = {'markerfacecolor': 'white', 'color': 'w', 'marker': 'o', 'markersize': '5'}
        entry = [Line2D([0], [0], **config, markeredgecolor='black', label='Total accuracy'),
            Line2D([0], [0], **config,  markeredgecolor = 'cyan', label='Left channel'),
            Line2D([0], [0], **config, markeredgecolor = 'magenta', label='Right channel')]
        return entry
