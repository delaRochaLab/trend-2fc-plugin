import numpy as np
import pandas as pd
import logging
from trend_2fc_session_plugin.exc_manager import log, method_log

class perfRecord:
    """
    Data holder class for session performances. It holds the raw data (1 or 0 for correct
    and incorrect), the rolling average of the raw data (window size dictated by the preset) and
    the indices (mandatory for left and right performances, which by necessity skip trials).
    """
    def __init__(self, window_size: int):
        self.raw_data = []
        self.mean = []
        self.index = []
        self.window_size = window_size

    def compute_mean(self):
        """
        Rolling average with a size equal to self.window_size.
        """
        if len(self.mean) < self.window_size + 1:
            self.mean.append(np.nanmean(self.raw_data))
        else:
            self.mean.append(np.nanmean(self.raw_data[-self.window_size:-1]))
    
    def add_element(self, perf, index):
        """
        Add a new result and compute the mean.
        """
        self.raw_data.append(perf)
        self.index.append(index)
        self.compute_mean()

    def __str_(self):
        return f'raw_data: {self.raw_data}, mean: {self.mean}, index: {self.index}'
        
class strToData:
    
    def __init__(self, preset):
        self.preset = preset  # Holds the plot presets coming from the main window.
        self.reward_side = None  # Current reward side, coming from the session data.
        self.evidences = []  # Contains the trial evidences, if any.
        self.silences = []  # Contains the session silences, if any.
        self.zt_threshold = 0
        self.zt_lights = []
        self.zt_no_lights = []
        self.trial_lights = []
        self.trial_no_lights = []
        self.colors = []   # Contains the dot colors for the upper display plot.
        self.onset_L = [] # Contains the L side latencies in ms for those setups that support sound detection.
        self.onset_R = [] # Contains the R side latencies in ms for those setups that support sound detection.
        self.missing_sounds = [] # Contains True if the sound was missing for that trial, else False.
        # These three variables hold the performance data. See the definition of the perfRecord class above:
        self.total_perf = perfRecord(self.preset['WINDOW_SIZE'])
        self.R_perf = perfRecord(self.preset['WINDOW_SIZE'])
        self.L_perf = perfRecord(self.preset['WINDOW_SIZE'])
        # Counter tracking number of trials:
        self.LAST_TRIAL = 0
        self.CHUNK_COUNTER = 0
        # Indices used to return a window of data; they should match start and end 
        # of the plotting window:
        self.start = 0
        self.end   = 0 


    @method_log
    def sound_detection(self, trial_band):
        """
        This function receives complete trials (by using the NEW TRIAL rows) and tries
        to find BNC transitions. If found, it computes mean onset and offset latencies. If not,
        a missing sound is assumed and displays it on the plot with a cross.
        """
        BNC = ('BNC1High','BNC1Low', 'BNC2High', 'BNC2Low')
        MSG = 'StartSound'
        if trial_band.size:
            # -------------------------------------
            trials = trial_band.query("TYPE=='TRIAL' and MSG=='New trial'").index
            start, end = trials[0], trials[-1]
            # --------- STATES COMPUTATION -----------
            bnc_events = trial_band[start:end].loc[(trial_band['+INFO'].isin(BNC) | (trial_band['MSG'].str.contains(MSG) & trial_band['TYPE'].str.contains('TRANSITION')))]
            grouper = bnc_events.groupby(pd.cut(bnc_events.index, bins=trials), as_index=False)
            for name, group in grouper:
                try:
                    L = float(group[group['+INFO'] == 'BNC1High']['BPOD-INITIAL-TIME'].values[0]) - float(group[group['MSG'] == 'StartSound']['BPOD-INITIAL-TIME'].values[0])
                except IndexError:
                    L = np.nan
                try:
                    R = float(group[group['+INFO'] == 'BNC2High']['BPOD-INITIAL-TIME'].values[0]) - float(group[group['MSG'] == 'StartSound']['BPOD-INITIAL-TIME'].values[0])
                except IndexError:
                    R = np.nan
                self.onset_L.append(L), self.onset_R.append(R)     
                if np.isnan(L) and np.isnan(R):
                    self.missing_sounds.append(self.CHUNK_COUNTER)
                self.CHUNK_COUNTER += 1

               
    @method_log
    def process_past_trials(self, trial_band):
        """
        This method receives a list of trials from the main window and updates all the 
        appropriate accuracies as well as the colors, according to the selected settings.
        """
        # --------- Update the performances: ----------------
        for state in trial_band:
            # Get the correct color from the preset:
            self.colors.append(self.preset['STATES'][state])
            if state in self.preset['ACC_UP']:
                perf = 1
            elif state in self.preset['ACC_DOWN']:
                perf = 0
            else:  # That is, the state is inside neutral perf:
                perf = np.nan
            self.total_perf.add_element(perf, self.LAST_TRIAL)
            if self.reward_side[self.LAST_TRIAL] == 1:  # Left side
                self.L_perf.add_element(perf, self.LAST_TRIAL)
                # Coherences equal to zero (if any) have to be displayed with a certain offset in the plot,
                # so that the side where they belong can be distinguished:
                if self.evidences and not self.evidences[self.LAST_TRIAL]:
                    self.evidences[self.LAST_TRIAL] += 0.02
            else:  # Right side
                if self.evidences and not self.evidences[self.LAST_TRIAL]:
                    self.evidences[self.LAST_TRIAL] -= 0.02
                self.R_perf.add_element(perf, self.LAST_TRIAL)
            self.LAST_TRIAL += 1

    @method_log
    def add_evidences(self, evidences: list):
        self.evidences += evidences

    @method_log
    def add_silences(self, silences: list):
        self.silences += silences

    @method_log
    def add_reward_side(self, reward_side):
        """ 
        Method that updates reward_side if it is found inside the new data query.
        In our plot, right trials are at the bottom and left trials at the top, so we swap the values
        from 0 to 1 and from 1 to -1.
        """
        reward_side = reward_side[1:-1].split(',')
        rew = [1 if elem.strip() == '0' else -1 for elem in reward_side]
        self.reward_side = rew

    def cut_performance(self, perf):
        """
        Method that receives a perfRecord object and returns the indices and values
        that fall between the start and end trials.
        """
        try:
            perf.index[-1]
        except IndexError:
            return [], []
        start = np.searchsorted(perf.index, self.start)
        end = np.searchsorted(perf.index, self.end)
        return perf.index[start:end], perf.mean[start:end]

    def trial_results(self, coherences=False):
        """
        Slice and return the indices, the values and the colors for the top plot on the main window.
        """
        try:
            self.total_perf.index[-1]
        except IndexError:
            return [], [], [], []
        end = self.LAST_TRIAL
        values = self.evidences[self.start:end] if coherences else self.reward_side[self.start:end]
        return self.total_perf.index[self.start:end], values, self.colors[self.start:end], self.silences[self.start:end]

    def cut_reward_side(self, evi_len):
        """
        Slice and return the reward side, that will be plotted in gray in the main window.
        Trial results in the appropriate colors will ovewrite the reward_side plot.
        """
        if not self.reward_side:
            return [], []
        length = self.start + evi_len
        return [idx for idx in range(length, self.end)], self.reward_side[length:self.end]

    @property
    def left_performance(self):
        """
        Return the cut left performance numbers, ready for the plot.
        """
        return self.cut_performance(self.L_perf)

    @property
    def right_performance(self):
        """
        Return the cut right performance numbers, ready for the plot.
        """
        return self.cut_performance(self.R_perf)

    @property
    def total_performance(self):
        """
        Slice and return the total performance indices and averaged values,
        ready for the plot on the main window.
        """
        try:
            self.total_perf.index[-1]
        except IndexError:
            return [], []
        end = self.start + self.LAST_TRIAL
        return self.total_perf.index[self.start:end], self.total_perf.mean[self.start:end]  

